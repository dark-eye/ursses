// Connect functions to an icubator object
function connectToIncubator(incubator, signalHandlers, readyCallback) {
	var componentData = [];
	incubator.onStatusChanged = function(status) {
		if (status == Component.Ready) {
			componentData.obj = incubator.object;
			for(var event in signalHandlers) {
				componentData.obj[event].connect(signalHandlers[event]);
			}
			if(readyCallback) {
				readyCallback(componentData, incubator);
			}
		}
	}
	return componentData;
}
