
import QtQuick 2.9
import Lomiri.Components 1.3
import QtQuick.Controls 2.2 as QControls


import "../Components"
import "../Components/UI"
import "../Components/UI/Fields"
import "../Jslibs/utils.js"  as UrssesUtils

Page {
	id:_manageFeed
	
	property var feedsModel: []
	
	clip:true
	
	header: PageHeader {
		id:header
		title:i18n.tr("Manage Feeds")
		trailingActionBar  {
						numberOfSlots:4
						actions : [
							Action {
								text : i18n.tr("Apply")
								iconName : "save"
								onTriggered : {
									for(var feed of root.urls) {
										if( _feedsList.model && feed &&  _feedsList.model.indexOf(feed) < 0) {
											console.log("Remove Feed : "+ feed);
											root.mainBillboard.removeFeed(feed);
										}
									}
									root.urls = _feedsList.model
									mainLayout.removePages(_manageFeed);
									root.primaryPageNotify(i18n.tr("Changes to feed list saved."));
								}
							},
							Action {
								text : i18n.tr("Add Feed")
								iconName : "list-add"
								onTriggered : {
									mainLayout.addPageToNextColumn(_manageFeed, Qt.resolvedUrl("AddRssPage.qml"),{})
								}
							},
							Action {
								enabled:false
							},
							Action {
								text : i18n.tr("Export feeds")
								iconName : "share"
								onTriggered : {
									root.mainBillboard.exportFeeds(_feedsList.model);
								}
							}
						]
		}
	}

	Connections {
		target:root.mainBillboard
		onFeedsChanged: {
			_manageFeed.feedsModel = feeds;
		}
	}
	
	Component {
		id: _tagMngComp
		ManagementPage {
			id:_tagMng
			pageHeader.title : i18n.tr("Manage Tags")
			property var tagAciton: Action {
				name:'add_tag'
				text:i18n.tr('Add tag')
				iconName:'add'
				onTriggered: {
					var modelToAddTo = list.model
					modelToAddTo.unshift('');
					list.model = modelToAddTo;
					console.log(list.model);
				}
			}
			Component.onCompleted: {
				_tagMng.headerActionBar.actions.push(tagAciton);
			}
			onDone : function(updatedModel) {
				urssesMainDB.updateTagsForFeed(parentData, updatedModel);
				mainLayout.removePages(_tagMng);
				root.currentPageNotify( i18n.tr("Changes saved."));
			}

			onCanceled : function() {
				mainLayout.removePages(_tagMng);
				root.currentPageNotify( i18n.tr("Changes cancelled."));
			}

			itemDelegate: SelectListItem {
				id:_tagEditDelegate
				width:parent.width;
				Component.onCompleted : {
					icon.color= urssesMainDB.getSectionValueForIndex("tags_color://"+modelData,'tags_color')
				}
				enterDataField.placeholderText:i18n.tr("Enter tag name")
				icon.name:"tag"
				enterDataField.text:modelData
				selectionModel:availibleOptions

				onChanged: {
					var data =  list.model;
					data[index] = text;
					list.model = data;
					console.log(text, index, JSON.stringify(list.model));
				}
				Item {
					id:colorPickerWrapper
					SlotsLayout.position: SlotsLayout.Trailing
				}
				trailingActions : ListItemActions {
					actions :  [
						Action {
							name: "Set Color"
							text: i18n.tr("Set Color")
							iconName:'preferences-color-symbolic'
							onTriggered: {
								var component = Qt.createComponent(Qt.resolvedUrl("../Components/UI/ColorPicker.qml"));

								var obj = component.createObject(_tagMng.pageHeader.customWidget,{width:_tagMng.pageHeader.width});
								if(!obj) {
									console.log( "Error loading ColorPicker:", component.errorString());
								}
								obj.onSelected.connect(function(color) {
									obj.destroy();
									icon.color = color;
									 urssesMainDB.updateSectionValuesForIndex("tags_color://"+modelData, ""+color,'tags_color')
								});
								obj.onCancelled.connect(function() {
									obj.destroy();
								});


							}
						}
					]
				}
				leadingActions : ListItemActions {
					actions : Action {
						name: "Delete"
						text: i18n.tr("Delete")
						iconName:'delete'
						onTriggered: {
							var data = list.model;
							data.splice(index,1);
							list.model = data;
						}
					}
				}
			}
		}
	}

	LomiriListView {
		id:_feedsList
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		model:_manageFeed.feedsModel
		delegate: ListItem {
			ListItemLayout {
				title.text : modelData
			}
			leadingActions : ListItemActions { 
				actions : Action {
					name: "Delete"
					text: i18n.tr("Delete")
					iconName:'delete'
					onTriggered: {
						_manageFeed.feedsModel.splice(index,1);
						_manageFeed.feedsModel = _manageFeed.feedsModel;
					}
				}
			}
			trailingActions : ListItemActions {
				actions: [
					Action {
					name: "Copy"
					text: i18n.tr("copy")
					iconName:'edit-copy'
					onTriggered: {
						 var mimeData = Clipboard.newData();
						 mimeData.text = modelData
						 mimeData.format = [ "text/uri-list","text/plain" ];
						Clipboard.push(mimeData)
						}
					},
					Action {
						name: "manage-tags"
						text: i18n.tr("Manage Tags")
						iconName:'tag'
						onTriggered: {
							var tags = urssesMainDB.getTagsForFeed(modelData);
							var allTags = urssesMainDB.getAllCategoriesTags();
							var incubator = mainLayout.addPageToCurrentColumn(_manageFeed, Qt.resolvedUrl("../Components/UI/TagsManagment.qml"),{ model:  tags && Array.isArray(tags)  ? tags : [], availibleOptions : allTags, parentData: modelData });
							var component = UrssesUtils.connectToIncubator(incubator,{},null)
					}
				}
				]
			}
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

