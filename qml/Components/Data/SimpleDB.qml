import QtQuick 2.9
import U1db 1.0 as U1db

Item {
	id:_feedCache
	property var dbName:"darkeye-ursses-main-db"
	property alias db:_mainDbInstance
	
	function getDoc(key, category) {
		var indexes = [ queryDocsByValue, queryDocsByRef] ; 
		for(var queryBy of indexes) {
			queryBy.query = [ key ];
			if(queryBy.results.length) {
				for(var i in queryBy.documents) {
					var foundDoc = _mainDbInstance.getDoc(queryBy.documents[i]);
					if(foundDoc && foundDoc.value && (!category || foundDoc.category == category)) {
						return foundDoc.value;
					}
				}
			}
		}
		return null;
	}
	
	function getDocs(filter,category) {
		filter = filter ? filter : "*"
		var results = [];
		var indexes = [ queryDocsByValue, queryDocsByRef] ; 
		for(var queryBy of indexes) {
			queryBy.query = [ filter ];
			if(queryBy.results.length) {
				for(var i in queryBy.documents) {
					var foundDoc = _mainDbInstance.getDoc(queryBy.documents[i]);
					if(foundDoc && foundDoc.value && (!category || foundDoc.category == category)) {
						results.push(foundDoc.value);
					}
				}
			}
		}
		return results;
	}
	
	function getFullDocs(filter,category) {
		filter = filter ? filter : "*"
		var results = {};
		var indexes = [ queryDocsByValue, queryDocsByRef] ; 
		for(var queryBy of indexes) {
			queryBy.query = [ filter ];
			if(queryBy.results.length) {
				for(var i in queryBy.documents) {
					var foundDoc = _mainDbInstance.getDoc(queryBy.documents[i]);
					if(foundDoc && foundDoc.value && (!category || foundDoc.category == category)) {
						results[queryBy.documents[i]] = foundDoc;
					}
				}
			}
		}
		return results;
	}
	
	function putDoc(value, category, reference, id) {
		category = category ? category : 'feed'
		reference = reference ? reference : value		
		return db.putDoc({"value": value, "ref":reference ,"category":category},id);
	}
	
	
	U1db.Database {
		id:_mainDbInstance
		path: dbName
	}

	U1db.Index {
		id:sourceByRefIndex
		database:_mainDbInstance
 		name:"sourceByRefIndex"
		expression: [ "value" ]
	}
	U1db.Index {
		id:sourceIndex
		database:_mainDbInstance
 		name:"sourceIndex"
		expression: [ "ref" ]
	}
	U1db.Query {
		id:queryDocsByValue
		index: sourceIndex
		query: ["*"]
	}
		U1db.Query {
		id:queryDocsByRef
		index: sourceByRefIndex
		query: ["*"]
	}
}
 /** Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



