
SimpleDB {
	
	function getAllCategoriesTags() {
		var results = this.getDocs("*", "tags");
		console.log(JSON.stringify(results));
		var allTagsHash = {};
		for(const tagsDocIdx in results) {
			if(!results[tagsDocIdx]) continue;
			for(const tagsIdx in results[tagsDocIdx]) {

				if( typeof(results[tagsDocIdx][tagsIdx]) === "string") {
					allTagsHash[results[tagsDocIdx][tagsIdx]] = 1;
				}
			}
		}

		return  Object.keys(allTagsHash);
	}
	
	function getTagsForFeed( feed ) {
		var results = this.getDocs(feed, "tags");
		var tagsModel =results.pop();
		console.log(JSON.stringify(tagsModel));
		return tagsModel && Array.isArray(tagsModel) ? tagsModel : [];
		
	}
	
	function updateTagsForFeed( feed, tags ) {
		var results = this.getFullDocs(feed, "tags");
		var docId = Object.keys(results).pop()
		console.log(results);
		console.log(tags);
		this.putDoc(tags,"tags",feed, docId);
	}

	function getAllCategoriesBySection(section) {
		var results = this.getDocs("*", section);

		var allSectionValuesHash = {};
		for(const sectionDocIdx in results) {
			if(!results[sectionDocIdx]) continue;
			for(const tagsIdx in results[sectionDocIdx]) {

				if( typeof(results[sectionDocIdx][tagsIdx]) === "string") {
					allSectionValuesHash[results[sectionDocIdx][tagsIdx]] = 1;
				}
			}
		}

		return  Object.keys(allSectionValuesHash);
	}

	function getSectionValueForIndex( feed , section) {
		var results = this.getDocs(feed, section);
		var tagsModel = results.pop();
		//console.log("DB value :" + JSON.stringify(tagsModel));
		return tagsModel ? tagsModel : [];

	}

	function updateSectionValuesForIndex( feed, values, section ) {
		var results = this.getFullDocs(feed, section);
		var docId = results ? Object.keys(results).pop() : null
		this.putDoc(values, section, feed, docId ? docId : null );

	}
}
