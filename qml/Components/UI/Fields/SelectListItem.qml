
import QtQuick 2.9
import Ubuntu.Components 1.3

ListItem {
	id:_root
	height:implicitHeight + units.gu(2)
	implicitHeight:_layout.implicitHeight + _otherOptions.height

	signal changed(string text)

	property alias icon : _icon;
	property alias layout : _layout;
	property alias enterDataField : _enterDataField;
	property var selectionModel : [];
	Column {
		width:parent.width

		ListItemLayout {
			id:_layout
			TextField {
				id:_enterDataField
				width:parent.width-units.gu(8);
				hasClearButton:true
				placeholderText:i18n.tr("Enter value")
				inputMethodHints: Qt.ImhNoPredictiveText
				onAccepted:  {
					_root.changed(text)
				}
			}
			Icon {
				id:_icon
				width: units.gu(2)
				name: "tag"
				SlotsLayout.position: SlotsLayout.Trailing
			}
		}
		ListView {
			id:_otherOptions
			anchors {
				bottomMargin:units.gu(1)
			}
			visible:_enterDataField.text.length > 2 && (_enterDataField.activeFocus || visible && _otherOptions.activeFocus ||visible && _root.activeFocus )
			model:selectionModel
			height: Math.min((visible ? units.gu(25) : 0 ), Math.max( units.gu(selectionModel.filter( i => { return i.match(new RegExp(_enterDataField.text,"i")); }).length * 2), units.gu(2)) )
			width: parent.width
			delegate: ListItem {
				height:visible ? units.gu(4) : 0;
				visible: modelData.match(new RegExp(_enterDataField.text,"i"));
				Label {
					anchors {
						verticalCenter:parent.verticalCenter
					}
					text: modelData
				}
				onClicked: {
					_enterDataField.text = modelData
					_root.changed(modelData)
				}
			}
		}
	}
}
