import QtQuick 2.9
import Ubuntu.Components 1.3

ListItem {
	property alias icon : _icon
	property alias layout : _layout
	
	ListItemLayout {
		id:_layout
		TextField {
			width:parent.width-units.gu(8);

			placeholderText:i18n.tr("Enter value")
			text:modelData
		}
		Icon {
			id:_icon
			width: units.gu(2)
			name: "tag"
			SlotsLayout.position: SlotsLayout.Trailing
		}
	}
}
