import QtQuick 2.9
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.9

Item {
	id: colorPicker
	height: units.gu(35)


	signal selected( color selectedColor );
	signal cancelled();

	property var selectedColor: "white"
	property var colors: ["red", "green", "blue", "yellow", "orange", "purple", "pink"]
	property var customColors: []

	Column {
		anchors.centerIn: parent
		width:parent.width - units.gu(20)
		spacing: 10

		Grid {
			id: colorGrid
			columns: 7
			spacing: units.gu(0.2)
			anchors.horizontalCenter:parent.horizontalCenter

			Repeater {
				model: colors.concat(customColors)

				Rectangle {
					width: units.gu(3.5)
					height: width
					color: modelData
					border.color: "transparent"
					border.width: colorGrid.selectedIndex == index ? 3 : 1

					MouseArea {
						anchors.fill: parent
						onClicked: {
							selectedColor = modelData
							console.log("Selected Color:", selectedColor)
						}
					}
				}
			}
		}


		Canvas {
			id: colorWheel
			width: Math.min(parent.width,units.gu(25))
			height: width
			anchors.topMargin: units.gu(0.5)
			anchors.horizontalCenter:parent.horizontalCenter

			onPaint: if( available ){
				var ctx = getContext("2d");
				var stepSize=5;
				for(var x=0;x < width;x+=stepSize) {
					for(var y=0; y < height; y+=stepSize) {
						var u = (x/width)-0.5;
						var v = (y/height)-0.5;
						ctx.fillStyle = Qt.hsla((Math.atan2(u,v)/(Math.PI*2))+0.5, Math.sqrt(u*u+v*v)*2, 1-Math.sqrt(u*u+v*v)*1.33);
						ctx.fillRect(x, y, stepSize, stepSize);
					}
				}
			}

			MouseArea {
				anchors.fill:parent
				onClicked: {
					var x = mouse.x;
					var y = mouse.y;
					try {
						var context = colorWheel.getContext("2d")
						console.log(x,y)
						var imageData = context.getImageData(x,y,1,1)
						console.log(imageData.data[0],imageData.data[1],imageData.data[2]);
						colorPicker.selectedColor = Qt.rgba(imageData.data[0]/256,imageData.data[1]/256,imageData.data[2]/256, 1.0);
					} catch (ex) {
						console.log(ex);
					}
					console.log("Selected Color:", colorPicker.selectedColor);
				}
			}
		}

		Button {
			anchors.horizontalCenter:parent.horizontalCenter
			width: colorWheel.width
			text: i18n.tr("Done")

			color:colorPicker.selectedColor
			onClicked: {
				selected(selectedColor);
			}
		}

	}
	Component.onCompleted : {
		// console.log("Color Picker Loaded");
	}
}
