import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

import "Fields"

Page {
	id:_mngPage
	
	signal done(var updatedModel);
	signal canceled();
	
	clip:true
	
	property alias itemDelegate:_itemsList.delegate;
	property alias list:_itemsList;
	property alias headerActionBar:_header.trailingActionBar;
	property alias pageHeader:_header;
	property alias model: _itemsList.model
	property var availibleOptions: []
	property var parentData: null
	
	header: PageHeaderWithBottomText {
		id:_header
        title: i18n.tr("Manage")
		trailingActionBar {
			actions: [
				Action {
					name:'ok'
					text:i18n.tr("OK")
					iconName:"ok"
					onTriggered: {
						_mngPage.done(_mngPage.model);
					}
				},
				Action {
					name:'cancel'
					text:i18n.tr("Cancel")
					iconName:"cancel"
					onTriggered: {
						_mngPage.canceled();
					}
				}
			]
		}
	}


	UbuntuListView {
		id:_itemsList

		anchors {
			fill:parent 
			topMargin:_header.height
		}
		model:[]
		delegate: SelectListItem {
			width:parent.width;

			enterDataField.text:modelData
			selectionModel:availibleOptions
			onChanged: {
				var data =  list.model;
				data[index] = text;
				list.model = data;
				console.log(text, index, JSON.stringify(list.model));
			}

			leadingActions : ListItemActions {
				actions : Action {
					name: "Delete"
					text: i18n.tr("Delete")
					iconName:'delete'
					onTriggered: {
						var data = list.model;
						data.splice(index,1);
						list.model = data;
					}
				}
			}
		}
	}
}
/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

