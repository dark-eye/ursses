import QtQuick 2.9
import Ubuntu.Components 1.3

import "../../Components"
import "../../Components/UI"
import "../../Components/UI/Fields"
import "../../Jslibs/utils.js"  as UrssesUtils

ManagementPage {
	id:_tagMng

	pageHeader.title : i18n.tr("Manage Tags")
	property var tagAciton: Action {
		name:'add_tag'
		text:i18n.tr('Add tag')
		iconName:'add'
		onTriggered: {
			var modelToAddTo = list.model
			modelToAddTo.unshift('');
			list.model = modelToAddTo;
			console.log(list.model);
		}
	}
	Component.onCompleted: {
		_tagMng.headerActionBar.actions.push(tagAciton);
	}
	onDone : function(updatedModel) {
		urssesMainDB.updateTagsForFeed(parentData, updatedModel);
		mainLayout.removePages(_tagMng);
		root.currentPageNotify( i18n.tr("Changes saved."));
	}

	onCanceled : function() {
		mainLayout.removePages(_tagMng);
		root.currentPageNotify( i18n.tr("Changes cancelled."));
	}

	itemDelegate: SelectListItem {
		id:_tagEditDelegate
		LayoutMirroring.enabled: false

		width:parent.width;
		Component.onCompleted : {
			icon.color= urssesMainDB.getSectionValueForIndex("tags_color://"+modelData,'tags_color')
		}
		enterDataField.placeholderText:i18n.tr("Enter tag name")
		icon.name:"tag"
		enterDataField.text:modelData
		selectionModel:availibleOptions

		onChanged: {
			var data =  list.model;
			data[index] = text;
			list.model = data;
			console.log(text, index, JSON.stringify(list.model));
		}
		Item {
			id:colorPickerWrapper
			SlotsLayout.position: SlotsLayout.Trailing
		}
		trailingActions : ListItemActions {
			actions :  [
				Action {
					name: "Set Color"
					text: i18n.tr("Set Color")
					iconName:'preferences-color-symbolic'
					onTriggered: {
						var component = Qt.createComponent(Qt.resolvedUrl("../../Components/UI/ColorPicker.qml"));
						// _tagMng.pageHeader.customWidget.visible = true;
						var obj = component.createObject(_tagMng.pageHeader.customWidget,{width:_tagMng.pageHeader.width});
						if(!obj) {
							console.log( "Error loading ColorPicker:", component.errorString());
						}
						obj.onSelected.connect(function(color) {
							obj.destroy();
							icon.color = color;
							urssesMainDB.updateSectionValuesForIndex("tags_color://"+modelData, ""+color,'tags_color')
						});
						obj.onCancelled.connect(function() {
							obj.destroy();
						});


					}
				}
			]
		}
		leadingActions : ListItemActions {
			actions : Action {
				name: "Delete"
				text: i18n.tr("Delete")
				iconName:'delete'
				onTriggered: {
					var data = list.model;
					data.splice(index,1);
					list.model = data;
				}
			}
		}
	}
}
